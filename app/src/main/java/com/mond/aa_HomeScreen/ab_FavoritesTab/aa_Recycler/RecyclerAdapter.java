package com.mond.aa_HomeScreen.ab_FavoritesTab.aa_Recycler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.geometry.mond.mond.R;
import com.mond.aa_HomeScreen.ab_FavoritesTab.Paramter_Parcelable;
import com.mond.aa_HomeScreen.home_screen;
import com.mond.aa_StartUp;
import com.mond.ab_FolderScreen.ab_FolderTab.FavoritesFolderFragment;
import com.mond.ab_FolderScreen.folder_screen;
import com.mond.dummy.MainActivity;
import com.mond.za_global.MyApplication;
import com.mond.zc_Glide.FutureStudioAppGlideModule;

import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Directory> directories;
    private Context context;



    public RecyclerAdapter(List<Directory> directories, Context context) {
        this.directories = directories;
        this.context = context;


    }

    public void setRecyclerAdapter(List<Directory> directories, Context context) {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ab_d_slice_frag_fav_recycler_item, viewGroup, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

       //if(i%2==0)
      //  viewHolder.video_thumb.setTextColor(Color.parseColor("#FFEBEE"));
       // viewHolder.video_thumb.setText("lol");
        Directory directory = this.directories.get(i);

        viewHolder.video_count.setText(directory.video_count);


        List<String> values = new ArrayList<String>();

        values = ((MyApplication) (MyApplication.getContext())).map.get(((MyApplication) (MyApplication.getContext())).names.get(i));
        viewHolder.dir_name.setText(directory.dir_name+"_"+i);

        Integer i5 = new Integer(i);
        Integer i6 = new Integer(0);
        if(i==0)
        {


            viewHolder.dir_name.setTextColor(Color.parseColor("#008000"));
             Log.d("#__INDEX", ((MyApplication) (MyApplication.getContext())).names.get(i)+" : "+i+":"+ values.get(0)+"  "+i5+":"+i6+";"+i5.equals(i6));

        }
        else{

            viewHolder.dir_name.setTextColor(Color.parseColor("#000000"));
        }

        int k = 0;
        int j = 0;
        if (k == 0) {
           // Log.d("#__INDEX", "onBindViewHolder: "+i+":"+ values.get(0)+i5+":"+i6+";"+i5.equals(i6));
        }

        viewHolder.slice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                List<String> values = new ArrayList<String>();

                values = ((MyApplication) (MyApplication.getContext())).map.get(((MyApplication) (MyApplication.getContext())).names.get(i));

                Paramter_Parcelable paramter_parcelable;
                paramter_parcelable = new Paramter_Parcelable(values.get(0),values.get(1));
                //Intent intent = new Intent(  v.getContext(), MainActivity.class);
               // intent.putExtra("data", 1);

               // v.getContext().startActivity(intent);
               // paramter_parcelable = new Paramter_Parcelable(text1,text1)
               // Intent myactivity = new Intent(context.getApplicationContext(),folder_screen.class);
                //myactivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
             //   myactivity.putExtra("DirectoryPath", paramter_parcelable);
              //  context.getApplicationContext().startActivity(myactivity);

             //   ((MyApplication) (MyApplication.getContext())).viewPager.setCurrentItem();

                ((MyApplication) (MyApplication.getContext())).map.clear();
                ((MyApplication) (MyApplication.getContext())).names.clear();

                FavoritesFolderFragment favoritesFolderFragment = (FavoritesFolderFragment) ((MyApplication) (MyApplication.getContext())).fragment_tabs.get(2);
               // ((MyApplication) (MyApplication.getContext())).adapter.swap(true);
               //  ((MyApplication) (MyApplication.getContext())).adapter.notifyDataSetChanged();

                favoritesFolderFragment.StartThread_files(values.get(0));
                ((MyApplication) (MyApplication.getContext())).adapter.notifyDataSetChanged();
                ((MyApplication) (MyApplication.getContext())).viewPager.setCurrentItem(2);

                ViewGroup slidingTabStrip = (ViewGroup) ((MyApplication) (MyApplication.getContext())).tabLayout.getChildAt(0);
                for (int i = 0; i < ((MyApplication) (MyApplication.getContext())).tabLayout.getTabCount(); i++) {
                    View tab = slidingTabStrip.getChildAt(i);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                    if(i==1)
                        layoutParams.weight = 0;
                    else
                        layoutParams.weight = 1;
                    tab.setLayoutParams(layoutParams);


                }


                //   ((MyApplication) (MyApplication.getContext())).adapter.removeFragment(1);


              //

                        //((MyApplication) (MyApplication.getContext())).viewPager.removeViewAt(1);

      /*          ((MyApplication) (MyApplication.getContext())).fragment_folder. add_refresh_listener(values.get(0));


             //   ((MyApplication) (MyApplication.getContext())).adapter.swap(false);
              //  ((MyApplication) (MyApplication.getContext())).adapter.notifyDataSetChanged();
                // values.add(dir.toString());
                /*
                values.add(count.toString());

                names.add(parts[parts.length - 1]);
                // names.add("WMV");
                map.put(parts[parts.length - 1], values);
*/
                Toast.makeText(context, "Item " + (i ) + " clicked  :: "+values.get(0),
                        Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return directories.size();
    }
}
