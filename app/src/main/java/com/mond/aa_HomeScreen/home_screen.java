package com.mond.aa_HomeScreen;

import android.Manifest;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import android.graphics.drawable.Drawable;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.TabLayout;

import com.geometry.mond.mond.R;
import com.mond.ab_FolderScreen.ab_FolderTab.FavoritesFolderFragment;
import com.mond.zb_shared_screen.aa_Tabs.aa_SpaceShareTab.SpaceShareFragment;
import com.mond.aa_HomeScreen.ab_FavoritesTab.FavoritesFragment;

import com.mond.za_global.MyApplication;
import com.mond.zb_shared_screen.ab_Pager.ViewPagerAdapter;


import android.os.Handler;

public class home_screen extends AppCompatActivity {
    static final  int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    public Toolbar toolbar;


    Handler handler;

    Context context;

    FavoritesFragment fragment_fav;

    Animation animation;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ab_a_activity_home_screen);
        String root = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
        context = this;
        ((MyApplication) (MyApplication.getContext())).fragment_tabs.add(new SpaceShareFragment());
       //
        ((MyApplication) (MyApplication.getContext())).fragment_tabs.add(  new FavoritesFragment());
        ((MyApplication) (MyApplication.getContext())).fragment_tabs.add(new FavoritesFolderFragment());


     //   Log.d("DIR_N", "map f: " + "::" + ((MyApplication) getApplication()).map.toString());


      //  Log.d("#FILEPATH", "onEvent: " + root);

        Window window = getWindow();
        Drawable background = getResources().getDrawable(R.drawable.ab_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        handler = new Handler(getApplicationContext().getMainLooper());


        getPermission();


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuCompat.setGroupDividerEnabled(menu, true);


        MenuInflater infl = getMenuInflater();
        infl.inflate(R.menu.main_menu, menu);


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.item1_id) {

            Toast.makeText(getApplicationContext(), "item1 is selected", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.item2_id) {

            Toast.makeText(getApplicationContext(), "item2 is selected", Toast.LENGTH_SHORT).show();


        } else if (id == R.id.item3_id) {

            Toast.makeText(getApplicationContext(), "item3 is selected", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);

    }


    public void enable_tab() {

        ((MyApplication) (MyApplication.getContext())).adapter = new ViewPagerAdapter(getSupportFragmentManager());
        ((MyApplication) (MyApplication.getContext())).adapter.addFragment(((MyApplication) (MyApplication.getContext())).fragment_tabs.get(0), "SpaceShare");
        ((MyApplication) (MyApplication.getContext())).adapter.addFragment(((MyApplication) (MyApplication.getContext())).fragment_tabs.get(1), "FAVORITES");
        ((MyApplication) (MyApplication.getContext())).adapter.addFragment(((MyApplication) (MyApplication.getContext())).fragment_tabs.get(2), "FOLDER");


        //   adapter.addFragment(((MyApplication) (MyApplication.getContext())).fragment_folder, "Folder");
        ((MyApplication) (MyApplication.getContext())).viewPager = findViewById(R.id.viewpager);
        ((MyApplication) (MyApplication.getContext())).viewPager.setAdapter(((MyApplication) (MyApplication.getContext())).adapter);

        ((MyApplication) (MyApplication.getContext())).tabLayout = findViewById(R.id.tab_layout);
        ((MyApplication) (MyApplication.getContext())).tabLayout.setupWithViewPager(((MyApplication) (MyApplication.getContext())).viewPager);
        ((MyApplication) (MyApplication.getContext())).viewPager.setCurrentItem(1);

        ViewGroup slidingTabStrip = (ViewGroup) ((MyApplication) (MyApplication.getContext())).tabLayout.getChildAt(0);
        for (int i = 0; i < ((MyApplication) (MyApplication.getContext())).tabLayout.getTabCount(); i++) {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            if(i==2)
                layoutParams.weight = 0;
            else
                layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);


        }

        //tabLayout.getTabAt(0).setText("Lol");
        //tabLayout.getTabAt(0).setCustomView(View.GONE);
      //  tabLayout.removeTabAt(2);


        //tabLayout.addTab(tabLayout.newTab().setCustomView(view));
        //tabLayout.getTabAt(0).getCustomView().setVisibility(View.GONE);


    }


    public void folder_tab() {
/*
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SpaceShareFragment(), "SpaceShare");
        adapter.addFragment(fragment_fav, "FAVORITES");

        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);

*/
    }


    public void getPermission() {

        if (ActivityCompat.checkSelfPermission(home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

          //  Toast.makeText(home_screen.this, "A", Toast.LENGTH_SHORT).show();

            enable_tab();


        } else {


            if (ActivityCompat.shouldShowRequestPermissionRationale(home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permission_snack_message();

            } else {
                ActivityCompat.requestPermissions(home_screen.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //                    grantResult[0] means it will check for the first postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(home_screen.this, "Permission Granted", Toast.LENGTH_SHORT).show();

                 //   Log.d("VIDEO FILES", "Sample");


                    StartThread_dir_update();
                    setFirstTime_permission();


                } else {


                    permission_snack_message();

                }

            }
        }


    }

    public void permission_snack_message() {
        Snackbar.make(findViewById(android.R.id.content),
                "Needs permission to read  video",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.requestPermissions(home_screen.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    }
                }).show();
    }


    public void StartThread_dir_update() {
      //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable() {
            public void run() {


                ((MyApplication) getApplication()).getVideo();


                try {
                    Thread.sleep(0);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {


                            enable_tab();
                        }
                    });


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
               // Log.i("#INSIDE", " thread id: " + Thread.currentThread().getId());


            }
        }).start();


    }


    private void setFirstTime_permission() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);

    //    Log.d("FIRST_HOME", "isFirstTime: " + ranBefore);


    }


}