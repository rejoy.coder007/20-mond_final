package com.mond;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.mond.aa_HomeScreen.home_screen;
import com.geometry.mond.mond.R;
import com.mond.za_global.MyApplication;

import java.io.File;


public class aa_StartUp extends AppCompatActivity   {

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getApplication()).map.clear();
        ((MyApplication) getApplication()).names.clear();


        Runnable r = new Runnable() {
            @Override
            public void run() {
                // if you are redirecting from a fragment then use getActivity() as the context.
                startActivity(new Intent(aa_StartUp.this, home_screen.class));
                // To close the CurrentActitity, r.g. SpalshActivity
                finish();
            }
        };

        handler = new Handler();


        if(isFirstTime())
        {
            setContentView(R.layout.aa_a_activity_start_up);

            handler.postDelayed(r, zz_Config.TIME_DELAY_START_SCREEN); // will be delayed for 1.5 seconds

        }
        else
        {

            setContentView(R.layout.aa_b_activity_splash_up);

           StartThread();
           // h.postDelayed(r, zz_Config.TIME_DELAY_FLASH_SCREEN); // will be delayed for 1.5 seconds

        }






    }




    private boolean isFirstTime()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        boolean ranBefore = preferences.getBoolean("Permission", false);
        Log.d("FIRST_START", "isFirstTime: "+ranBefore);
        return !ranBefore;
    }

    public void StartThread()
    {
       // Toast.makeText(this, "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable()
        {
            public void run()
            {




                try
                {
                    Thread.sleep(0);
                   // File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
                   // ((MyApplication) getApplication()).getfile(dir);
                  //  getfile(dir);

                    ((MyApplication) (aa_StartUp.this). getApplication()).getVideo();

                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {

                            //  recyclerView.setAdapter(new RecyclerAdapter(directories,getContext()));
                            //  displayTextView.setText(":"+count);
                           startActivity(new Intent(aa_StartUp.this, home_screen.class));
                            // To close the CurrentActitity, r.g. SpalshActivity
                            finish();
                        }
                    });

                    // count++;
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                Log.i("#THREAD", " thread id: " + Thread.currentThread().getId());
            }

        }).start();



    }



}
