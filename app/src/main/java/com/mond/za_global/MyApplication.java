package com.mond.za_global;

import android.app.Application;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.mond.ab_FolderScreen.ab_FolderTab.FavoritesFolderFragment;
import com.mond.zb_shared_screen.ab_Pager.ViewPagerAdapter;
import com.squareup.leakcanary.LeakCanary;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApplication extends Application {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public ViewPagerAdapter adapter;
    public FavoritesFolderFragment fragment_folder;
    public List<String> names = new ArrayList<String>();
    private static MyApplication mContext;
    // HashMap<String, String> map = new HashMap<>();
    public List<Fragment> fragment_tabs = new ArrayList<Fragment>();

    public Map<String, List<String>> map = new HashMap<String, List<String>>();



    public void getfile(File dir)
    {



        File listFile[] = dir.listFiles();
        Boolean video_flag=false;
        Integer count=0;

        if (listFile != null && listFile.length > 0)
        {


            for (int i = 0; i < listFile.length; i++)
            {

                if (listFile[i].isDirectory())
                {
                    //fileList.add(listFile[i]);
                    getfile(listFile[i]);

                }
                else
                {


                    if (
                            listFile[i].getName().endsWith(".3gp")
                                    || listFile[i].getName().endsWith(".avi")
                                    || listFile[i].getName().endsWith(".flv")
                                    || listFile[i].getName().endsWith(".m4v")
                                    || listFile[i].getName().endsWith(".mkv")
                                    || listFile[i].getName().endsWith(".mov")

                                    || listFile[i].getName().endsWith(".mp4")
                                    || listFile[i].getName().endsWith(".mpeg")
                                    || listFile[i].getName().endsWith(".mpg")
                                    || listFile[i].getName().endsWith(".mts")
                                    || listFile[i].getName().endsWith(".vob")



                                    || listFile[i].getName().endsWith(".webm")
                                    || listFile[i].getName().endsWith(".wmv")


                            )

                    // if (listFile[i].getName().endsWith(".webm"))

                    {


                        video_flag=true;
                        count++;



                        // names.add(listFile[i].toString());
                        //  mAttachmentList.add(new AttachmentModel(listFile[i].getName()));
                    }
                }
            }

            if(video_flag)
            {
                String string_path = dir.toString();
                String[] parts = string_path.split("/");

                List<String> values = new ArrayList<String>();
                values.add(dir.toString());
                values.add(count.toString());

                names.add(parts[parts.length - 1]);
                // names.add("WMV");
                map.put(parts[parts.length - 1], values);

                // map.put("WMV", values);
                //Log.d("DIR_N", "getfile: "+dir.toString()+"::"+Arrays.toString(parts));
                // Log.d("DIR_N", "getfile: "+dir.toString()+"::"+names.toString());
                // Log.d("DIR_N", "getfile: "+dir.toString()+"::"+map.toString());
            }







        }
        //return fileList;
    }

    private void getInbox()
    {
        // holder.text.setTextColor(Color.RED);



        List<String> values_1 = new ArrayList<String>();
        values_1.add("Inbox.path");
        values_1.add("12");
        names.add("Inbox");
        map.put("Inbox", values_1);

        List<String> values_2 = new ArrayList<String>();
        values_2.add("Inbox.path");
        values_2.add("12");
        names.add("Private Folder");

        map.put("Private Folder", values_2);

    }

    public void getVideo()
    {
        names.clear();
        map.clear();
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());


        getInbox();


        List<String> values = new ArrayList<String>();


        for(int i=0;i<0;i++){

            names.add("Inbox"+i);
            values.clear();
            values.add("Inbox.path");
            values.add("12");
            map.put(names.get(i), values);


        }




        getfile(dir );

        //Log.d("DIR_N", "getfile: "+dir.toString()+"::"+Arrays.toString(parts));
       // Log.d("DIR_N", "names: "+dir.toString()+"::"+names.toString());
     //   Log.d("DIR_N", "map: "+dir.toString()+"::"+map.toString());
    }


    public void getFilesFromDir(String file_root)
    {
        names.clear();
        map.clear();
        File dir = new File(file_root);


        File listFile[] = dir.listFiles();


        if (listFile != null && listFile.length > 0)
        {


            for (int i = 0; i < listFile.length; i++)
            {

                if (!listFile[i].isDirectory())
                {



                    if (
                            listFile[i].getName().endsWith(".3gp")
                                    || listFile[i].getName().endsWith(".avi")
                                    || listFile[i].getName().endsWith(".flv")
                                    || listFile[i].getName().endsWith(".m4v")
                                    || listFile[i].getName().endsWith(".mkv")
                                    || listFile[i].getName().endsWith(".mov")

                                    || listFile[i].getName().endsWith(".mp4")
                                    || listFile[i].getName().endsWith(".mpeg")
                                    || listFile[i].getName().endsWith(".mpg")
                                    || listFile[i].getName().endsWith(".mts")
                                    || listFile[i].getName().endsWith(".vob")



                                    || listFile[i].getName().endsWith(".webm")
                                    || listFile[i].getName().endsWith(".wmv")


                            )



                    {

                      //  Log.d("_DIR_PATH", "getFilesFromDir: "+listFile[i].getName());


                        List<String> values = new ArrayList<String>();
                        values.add(listFile[i].getName());
                        values.add(dir.toString()+"/"+listFile[i].getName());

                        names.add(listFile[i].getName());
                        // names.add("WMV");
                        map.put(listFile[i].getName(), values);

                    }
                }
            }






        }





    }



    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
/*
        Log.d("_FRAG_READY", "onCreate: ");

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...
        */

    }

    public static MyApplication getContext() {
        return mContext;
    }



}

