package com.mond.zb_shared_screen.ab_Pager;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mond.za_global.MyApplication;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List<String> fragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }

    public void removeFragment( int position) {
        fragmentList.remove(position);
        fragmentTitleList.remove(position);
    }

    public void swap(Boolean folder) {




        if(folder)
        {
            Fragment  fragment = ((MyApplication) (MyApplication.getContext())).fragment_tabs.get(2);
           // fragmentList.set(1,fragmentList.get(j));
            fragmentList.set(1,     fragment );
            fragmentTitleList.set(1,"Folder View");
        }
        else
        {
            Fragment  fragment = ((MyApplication) (MyApplication.getContext())).fragment_tabs.get(1);
            // fragmentList.set(1,fragmentList.get(j));
            fragmentList.set(1,     fragment );
            fragmentTitleList.add("Favorites");
        }




    }
}
