package com.mond.zc_Glide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.geometry.mond.mond.R;
import com.mond.ab_FolderScreen.ab_FolderTab.aa_Recycler.ViewHolder;

@GlideModule
public class FutureStudioAppGlideModule extends AppGlideModule {


    public void  add_image_to_view(final Context context, ViewHolder viewHolder, Uri uri)
    {

        final Boolean[] errorF = {false};

        GlideApp
                .with(context)

                .load(uri)
               // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.file_error))
               // .override(150, 150) // resizes the image to these dimensions (in pixel)
            //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(viewHolder.video_thumb);

      //  if(errorF[0])
      //  GlideApp.with(context).load(R.drawable.file_error);



       // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolder.video_thumb.getBackground().equals(R.drawable.file_error));



    }



}